
import edit from './edit';
import save from './save';
import deprecated from './deprecated';
import icon from './icon.svg.js';

import blockData from './block.json';

import {
	registerBlockType,
} from '@wordpress/blocks';

registerBlockType( blockData.name, {
	...blockData,
	icon,
	edit,
	save,
	deprecated,
});
